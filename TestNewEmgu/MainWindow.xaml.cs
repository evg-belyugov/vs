﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;

namespace TestNewEmgu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Переменные для видео
        private Capture _capture1;

        public MainWindow()
        {
            InitializeComponent();

            //_capture1 = new Capture("rtsp://admin:Magistral123@10.224.18.122:554");
            _capture1 = new Capture("http://188.170.32.93:82/mjpg/video.mjpg");
            _capture1.ImageGrabbed += ProcessFrame;
            _capture1.Start();
        }

        // Обработка кадра --------------------------------------------------
        //[HandleProcessCorruptedStateExceptions]
        private void ProcessFrame(object sender, EventArgs arg)
        {
            // Test 3

            if (_capture1 == null || _capture1.Ptr == IntPtr.Zero) return;

            Mat frame = new Mat();
            _capture1.Retrieve(frame, 0);
            if (!frame.IsEmpty) ImageBox1.Image = frame;
        }
    }
}
